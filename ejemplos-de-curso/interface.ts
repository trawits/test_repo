// Se crear un tipo de dato con el Type
type Dni = number;

//Se crea un interface que es como un objeto compuesto por varios tipos de atributos.
// En esta interface todos los campos son obligatorios.
interface Intf_Persona {
    altura: number;
    edad: number;
    nombre: string;
    apellido: string;
    dni: Dni; // se asigna el tipo Dni creado previamente
}


// En esta interface todos los campos NO son obligatorios.
interface Intf_Persona_2 {
    altura?: number;
    edad?: number;
    nombre?: string;
    apellido?: string;
    dni?: Dni; // se asigna el tipo Dni creado previamente
}


//se declara constant para asignar la interface creada.
const persona2: Intf_Persona ={
    altura: 1.87,
    edad:36,
    nombre: 'Stiwart',
    apellido:'Antúnez',
    dni: 24454602
}

//se declara constant para asignar la interface creada.
const persona_2: Intf_Persona_2 ={
    dni: 24454602
}
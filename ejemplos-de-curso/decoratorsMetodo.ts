// funcion creada para el decorator
function log(target, key){
    console.log(key + 'se llamado');
}

// clase
class Person2a {
    private name : string;
    constructor(name :string) {
        this.name = name;
    }
// aplicamos el decorator con el arroba implementado a metodos
// @log
// sayMy2Name(){
//     console.log(this.name);
// }
}

//Instancio con un obeto
const person2a : Person2a = new Person2a ('Stiwart');

// person2a.sayMy2Name();//Se hace el llamado.

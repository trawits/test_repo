// Creamos una clase
class Persona {
    //Variables
    private edad : number;
    private altura :number;
    private dni :string;
    
    //Creamos constructor
    constructor (edad:number, altura:number, dni:string){
        this.edad = edad;
        this.altura = altura;
        this.dni = dni;
    }
}

// Creamos otra clase que extiende de la clase Persona
class Alumno extends Persona{
    //Creamos atributo propio de esta clase Alumno
    private matricula :string;

    //Creamos constructor
    constructor (edad:number, altura:number, dni:string, matricula:string){
        super(edad,altura,dni);
        this.matricula = matricula;
    }
}

let l_persona : Persona = new Persona (27, 1.69,'24585377-7');
let l_alumno : Alumno = new Alumno (27, 1.69,'24585377-7','8555');

l_persona = l_alumno;
//l_alumno = l_persona;

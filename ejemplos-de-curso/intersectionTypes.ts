interface Interface_1 {
    prop1 : number;
}

interface Interface_2 {
    prop2 : number;
    prop3 : number;
}

// Con el & se indica un OR y es necesario informar todos los atributos de todas las interfaces asignadas el type
type Interface_Mix = Interface_1 & Interface_2;

const InterfaceMix : Interface_Mix = {
prop1 : 1,
prop2 : 2,
prop3 : 4
}
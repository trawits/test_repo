type CallBackError = Error | null;
type CallBack = (error:CallBackError, response: Object) => void;
type SendRequest = (cd : CallBack) => void;

// funcion
function sendRequest(cb:CallBack):void {
    if (cb) {
        cb(null,{ message:'Todo está OK' });
    }
}

// Aqui la funcion la convertimos o lo hacemos con un objeto const y con arrow function
const sendRequest_2 : SendRequest = (cb:CallBack):void =>{
    if (cb) {
        cb(null,{ message:'Todo está OK' });
    }
}


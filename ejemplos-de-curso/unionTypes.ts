// Los union type es que una varible hereder dos tipos de datos
type SumaParameter = string | number; //Este tipo puede ser de ambos
type SumaReturn = string | number; //Este tipo puede ser de ambos

function Suma(num1:SumaParameter, num2:SumaParameter): SumaReturn {
    return Number(num1) + Number(num2);
}

// Aplicandolo a Interface
 interface Interface1{
    prop1:number;
}

// Aplicandolo a Interface
interface Interface2{
    prop2:number;
}

// Definimos un tipo asociando los interfaces creados.
type InterfaceMix = Interface1 | Interface2;

// Creamos un objeto que contendra el tipo InterfaceMix
const ObjInterfaceMix : InterfaceMix ={
    prop1:2,
    prop2:4
}
